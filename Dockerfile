FROM bash:latest

MAINTAINER Hristo Hristov hristo.dr.hristov@gmail.com

RUN apk update && apk upgrade && apk add --no-cache p7zip
